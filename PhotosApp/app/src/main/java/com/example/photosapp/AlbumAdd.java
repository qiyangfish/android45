package com.example.photosapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AlbumAdd extends AppCompatActivity {

    public static final String AlBUM_NAME = "album_name";
    public static final String ALBUM_INDEX = "album_index";
    private EditText albumName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Create a new album");
        setContentView(R.layout.album_add);
        albumName = findViewById(R.id.album_add_name);

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void save(View view) {
        //BUG: Duplicate Album Name
        String name = albumName.getText().toString();
        if (name == null || name.length() == 0 ) {
            DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        break;
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Please type a name for your album")
                    .setPositiveButton("Ok", dialogClickListener)
                    .show();
            return;
        }

        for (int i = 0; i < AlbumsList.user.getAlbums().size(); i++){
            if (name.compareTo(AlbumsList.user.getAlbums().get(i).getName()) == 0){
                Toast.makeText(this,"Duplicate Name",Toast.LENGTH_SHORT).show();
                albumName.setText("");
                return;
            }
        }
        Bundle bundle = new Bundle();
        bundle.putString(AlBUM_NAME,name);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);
        finish(); // pops activity from the call stack, returns to parent
    }
}
