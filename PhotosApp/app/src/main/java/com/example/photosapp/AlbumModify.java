package com.example.photosapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import java.io.IOException;

public class AlbumModify extends AppCompatActivity {

    public static final String AlBUM_NAME = "album_name";
    public static final String ALBUM_INDEX = "album_index";
    private int AlbumListIndex;
    private EditText albumName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // get the fields
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_modification);
        albumName = findViewById(R.id.album_modify_name);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // see if info was passed in to populate fields
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            AlbumListIndex = bundle.getInt(ALBUM_INDEX);
            albumName.setText(bundle.getString(AlBUM_NAME));
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void save(View view) {
        // gather all data from text fields
        String name = albumName.getText().toString();
        //BUG: Duplicate Album Name
        if (name == null || name.length() == 0 ) {
            DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        break;
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Please type a name for your album")
                    .setPositiveButton("Ok", dialogClickListener)
                   .show();
            return;
        }

        for (int i = 0; i < AlbumsList.user.getAlbums().size(); i++){
            if (name.compareTo(AlbumsList.user.getAlbums().get(i).getName()) == 0){
                Toast.makeText(this,"Duplicate Name",Toast.LENGTH_SHORT).show();
                albumName.setText("");
                return;
            }
        }
        // make Bundle
        Bundle bundle = new Bundle();
        bundle.putInt(ALBUM_INDEX, AlbumListIndex);
        bundle.putString(AlBUM_NAME,name);
        //Log.d("tag", name);
        // send back to caller
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);
        finish(); // pops activity from the call stack, returns to parent
    }
}
