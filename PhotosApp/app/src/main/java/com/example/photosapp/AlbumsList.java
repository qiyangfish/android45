package com.example.photosapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class AlbumsList extends AppCompatActivity {

    private ListView listView;
    private String[] albumsNames;  //We no longer get this String array from strings.xml,instead, we can read from dat file now
    private ArrayList<ModelAlbum> albums;
    public static User user;
    public static final String AlBUM_NAME = "album_name";
    public static final String ALBUM_INDEX = "album_index";

    public static final int EDIT_ALBUM_CODE = 1;
    public static final int ADD_ALBUM_CODE = 2;
    public static final int SEARCH_ALBUM_CODE =3;
    //MUST HAVE
    public static final String EXTRA_INITIAL_URI="android.provider.extra.INITIAL_URI";

    private Boolean icon_isOpen = false;
    //Trying serial here
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //ModelAlbum aAlbum = new ModelAlbum();

        listView = findViewById(R.id.albums_list);


        //Load from dat file
        File newFile = new File("/data/data/com.example.photosapp/file/photos.dat");
        if (!newFile.exists()){
            try {
                newFile.createNewFile();
                //Toast.makeText(this,"New File Created",Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        user = new User();
        if (newFile.length() == 0) {
            //Toast.makeText(this,"File empty",Toast.LENGTH_SHORT).show();
            try {
                ArrayList<ModelAlbum> hello = new ArrayList<>();
                hello.add(new ModelAlbum("dafault"));
                user = new User();
                user.setAlbums(hello);
                User.write(user);
                albumsNames = new String[user.getAlbums().size()];
                for (int i = 0; i < user.getAlbums().size(); i++){
                    albumsNames[i] = user.getAlbums().get(i).getName();
                }
                Log.d("albumsNamesSize", Integer.toString(albumsNames.length));
            } catch (IOException e) {
                Toast.makeText(this,"Something Going Wrong",Toast.LENGTH_SHORT).show();
            }
        }
        else {
            try {
                user = User.read();
                albumsNames = new String[user.getAlbums().size()];
                for (int i = 0; i < user.getAlbums().size(); i++){
                    albumsNames[i] = user.getAlbums().get(i).getName();
                }
            } catch (IOException | ClassNotFoundException e) {
                Toast.makeText(this,"Something Going Wrong",Toast.LENGTH_SHORT).show();
            }
        }


        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, R.layout.album_list_textfield, albumsNames);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(
                (p,v,pos,id) -> albumView(pos));
        //for renaming
        listView.setOnItemLongClickListener(
                (p,v,pos,id) -> albumModify(pos));

        Animation fab_open, fab_close, fab_clock, fab_anticlock;
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_clock = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_rotate_clock);
        fab_anticlock = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_rotate_anticlock);

        FloatingActionButton fab = findViewById(R.id.fab);

        //fab is the + icon
        fab.setOnClickListener(view -> {
            //Toast.makeText(getApplicationContext(), "Add Album", Toast.LENGTH_SHORT).show();
            albumAdding();
        });

        FloatingActionButton fab_search = findViewById(R.id.fab_search);

        //fab is the + icon
        fab_search.setOnClickListener(view -> {
            //Toast.makeText(getApplicationContext(), "Search Photos", Toast.LENGTH_SHORT).show();
            albumSearching();
        });

        FloatingActionButton fab_show = findViewById(R.id.fab_show);

        fab_show.setOnClickListener(view -> {
            if (icon_isOpen) {
                //textview_mail.setVisibility(View.INVISIBLE);
                //textview_share.setVisibility(View.INVISIBLE);
                fab.startAnimation(fab_close);
                fab_search.startAnimation(fab_close);
                fab_show.startAnimation(fab_anticlock);
                fab_search.setClickable(false);
                fab.setClickable(false);
                icon_isOpen = false;
            } else {
                //textview_mail.setVisibility(View.VISIBLE);
                //textview_share.setVisibility(View.VISIBLE);
                fab.startAnimation(fab_open);
                fab_search.startAnimation(fab_open);
                fab_show.startAnimation(fab_clock);
                fab_search.setClickable(true);
                fab.setClickable(true);
                icon_isOpen = true;
            }
        });
    }

    private boolean albumSearching() {
        Bundle bundle = new Bundle();
        bundle.putString(AlBUM_NAME, "");
        Intent intent = new Intent(this, PhotoSearch.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, SEARCH_ALBUM_CODE);
        return true;
    }


    private boolean albumAdding() {
        Bundle bundle = new Bundle();
        bundle.putString(AlBUM_NAME, "");
        //bundle.putString(ROUTE_DETAIL,albumsDetails[pos]);
        Intent intent = new Intent(this, AlbumAdd.class);
        intent.putExtras(bundle);
        //calling onActivityResult when child finish
        startActivityForResult(intent, ADD_ALBUM_CODE);
        return true;
    }

    private void albumView(int pos) {
        Bundle bundle = new Bundle();
        bundle.putString(AlBUM_NAME,albumsNames[pos]);
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setClass(this, ThumbnailView.class);
        intent.setType("image/*");
        intent.putExtras(bundle);
        intent.putExtra(ALBUM_INDEX,pos);
        startActivity(intent);
    }

    private boolean albumModify(int pos) {
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    //Change button clicked
                    Bundle bundle = new Bundle();
                    bundle.putInt(ALBUM_INDEX, pos);
                    bundle.putString(AlBUM_NAME, albumsNames[pos]);
                    //bundle.putString(ROUTE_DETAIL,albumsDetails[pos]);
                    Intent intent = new Intent(this, AlbumModify.class);
                    intent.putExtras(bundle);
                    //calling onActivityResult when child finish
                    startActivityForResult(intent, EDIT_ALBUM_CODE);
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //Delete clicked
                    user.getAlbums().remove(pos);
                    //}
                    //global adapter
                    try {
                        User.write(AlbumsList.user);
                        user = User.read();
                        albumsNames = new String[user.getAlbums().size()];
                        for (int i = 0; i < user.getAlbums().size(); i++){
                            albumsNames[i] = user.getAlbums().get(i).getName();
                        }
                    } catch (IOException | ClassNotFoundException e) {
                        Toast.makeText(this,"Something Going Wrong",Toast.LENGTH_SHORT).show();
                    }
                    ArrayAdapter<String> adapter =
                            new ArrayAdapter<>(this, R.layout.album_list_textfield, albumsNames);
                    listView.setAdapter(adapter);
                    break;
                case DialogInterface.BUTTON_NEUTRAL:
                    //No button clicked
                    break;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("What would you like to do with this album?").setPositiveButton("Rename it", dialogClickListener)
                .setNegativeButton("Delete it", dialogClickListener).setNeutralButton("None", dialogClickListener).show();
        return true;

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            return;
        }

        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }

        // gather all info passed back by launched activity
        String name = bundle.getString(AlbumModify.AlBUM_NAME);
        int index = bundle.getInt(AlbumModify.ALBUM_INDEX);

        if (requestCode == EDIT_ALBUM_CODE) {
            albumsNames[index] = name;
        }
        if (requestCode == SEARCH_ALBUM_CODE){
            albumsNames = new String[user.getAlbums().size()];
            for (int i = 0; i < user.getAlbums().size(); i++){
                albumsNames[i] = user.getAlbums().get(i).getName();
            }
        }
        else {
            user.getAlbums().add(new ModelAlbum(name));
            albumsNames = new String[user.getAlbums().size()];
            for (int i = 0; i < user.getAlbums().size(); i++){
                albumsNames[i] = user.getAlbums().get(i).getName();
            }
            try {
                User.write(user);
            } catch (IOException e) {
                Toast.makeText(this,"Going Wrong Here",Toast.LENGTH_SHORT).show();
            }
        }
        // redo the adapter to reflect change
        listView.setAdapter(new ArrayAdapter<>(this, R.layout.album_list_textfield, albumsNames));
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search_photo) {
            Toast.makeText(this,"Search Clicked",Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */
}
