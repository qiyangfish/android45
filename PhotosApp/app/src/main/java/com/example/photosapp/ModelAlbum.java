package com.example.photosapp;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Define an album class for saving an album's name and list of "Photo"s
 * @author Qiyang Zhong, Shangda Wu
 */
public class ModelAlbum implements Serializable {
    private String AlbumsName;
    private ArrayList<ModelPhoto> photos;

    /**
     * constructor for a new Album
     * @param name name of the album
     */
    public ModelAlbum(String name) {
        this.AlbumsName = name;
        photos = new ArrayList<ModelPhoto>();
    }

    /**
     * Sets the name of this album
     * @param name the new name of this album
     */
    public void setName(String name) {
        this.AlbumsName = name;
    }

    public void setPhotos(ArrayList<ModelPhoto> photos) {
        this.photos = photos;
    }

    /**
     * Gets the name of this album
     * @return the name of this album
     */
    public String getName() {
        return this.AlbumsName;
    }

    /**
     * Gets the photos in this album
     * @return an arraylist of photos
     */
    public ArrayList<ModelPhoto> getPhotos() {
        return this.photos;
    }

    /**
     * Returns the number of photos in this album
     * @return the number of photos in this album
     */
    public int getPhotoCount() {
        return this.photos.size();
    }

    /**
     * Returns a string representation of this album
     */
    public String toString() {
        return "NAME: " + AlbumsName + "\nPHOTO COUNT: " + photos.size();
    }

}
