package com.example.photosapp;

import android.graphics.Bitmap;
import android.media.Image;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

/**
 * Define an Photo class for saving an photo's image, which can be displayed, caption, date, and list of tags
 * @author Qiyang Zhong, Shangda Wu
 */
public class ModelPhoto implements Serializable {

    private String uriPath;
    private Map<String, ArrayList<String>> tagsHashTable = new HashMap<>();
    //private PhotoToRGB rgb = new PhotoToRGB();

    public ModelPhoto(String uriPath) {
        this.uriPath = uriPath;
        //this.fileName = uriPath.substring(uriPath.lastIndexOf('/')+1);
    }

    public Map<String, ArrayList<String>> getMap(){
        return tagsHashTable;
    }

    public String getUriPath() {
        return uriPath;
    }

    public String[] getTag(){
        ArrayList<String> tagTemp = new ArrayList<String>();
        for(String key : tagsHashTable.keySet() ){
            ArrayList<String> arrayList = tagsHashTable.get(key);
            for (int i = 0; i < arrayList.size(); i++){
                tagTemp.add(key + ": " + arrayList.get(i));
            }
        }
        String[] tagMap = new String[tagTemp.size()];
        for(int j =0;j<tagTemp.size();j++){
            tagMap[j] = tagTemp.get(j);
        }

        return tagMap;
    }
    public void addTag(String key, String value){
        if (tagsHashTable.containsKey(key)){
            if(key.equals("Location")){
                return;
            }
            if (tagsHashTable.get(key).contains(value)) {
                return;
            }
            tagsHashTable.get(key).add(value);
        } else {
            ArrayList<String> arrList = new ArrayList<>();
            arrList.add(value);
            tagsHashTable.put(key, arrList);
        }
    }

    public void removeTag(String key, String value){
        tagsHashTable.get(key).remove(value);
        if(tagsHashTable.get(key).size()==0){
            tagsHashTable.remove(key);
        }
    }

    public ModelPhoto getSearchResult(String key, String value){
        ModelPhoto ans = null;
        ArrayList<String> arrayList = tagsHashTable.get(key);
        if (arrayList == null){
            return null;
        }
        for (int i = 0; i < arrayList.size(); i++){
            if (checkCorrectInput(arrayList.get(i),value)){
                ans = new ModelPhoto(uriPath);
                break;
            }
        }
        return ans;
    }

    private boolean checkCorrectInput (String key, String value){
        String existedTag = key.toLowerCase();
        String input = value.toLowerCase();
        return existedTag.contains(input);
    }
}
