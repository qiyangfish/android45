package com.example.photosapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

public class MovePhoto extends AppCompatActivity {

    public static final String PHOTO_INDEX = "photo_index";
    public static final String PHOTO_URL = "photo_url";
    public static final String ALBUM_INDEX = "album_index";
    public static final String PHOTO_LOCATION_TAG = "photo_location_TAG";
    public static final String PHOTO_PERSON_TAG = "photo_person_tag";


    private ArrayList<String> personTag;
    private ArrayList<String> locationTag;
    private String photoUrl;
    private int albumIndex;
    private int photoIndex;
    private ListView listViewMove;
    private ArrayList<String> albumNames = new ArrayList<>();
    private int itemIndex;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_move);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Move photo to...");
        itemIndex = -1;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            personTag = bundle.getStringArrayList(PHOTO_PERSON_TAG);
            locationTag = bundle.getStringArrayList(PHOTO_LOCATION_TAG);
            photoUrl = bundle.getString(PHOTO_URL);
            albumIndex = bundle.getInt(ALBUM_INDEX);
            photoIndex = bundle.getInt(PHOTO_INDEX);
        }
        listViewMove = findViewById(R.id.listView_move);

        for (int i = 0; i < AlbumsList.user.getAlbums().size(); i++){
            albumNames.add(AlbumsList.user.getAlbums().get(i).getName());
        }

        ArrayAdapter<String> testAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, albumNames);
        listViewMove.setAdapter(testAdapter);
        listViewMove.setOnItemClickListener(
                (p,v,pos,id) -> itemClicked(pos));
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void itemClicked(int pos) {
        itemIndex = pos;
        //String output = "You chose " + albumNames.get(itemIndex);
        //Toast.makeText(this,output,Toast.LENGTH_SHORT).show();
    }

    public void move_cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void move_confirm(View view){
        //Toast.makeText(this,"In Here",Toast.LENGTH_SHORT).show();
        if (itemIndex == -1){
            Toast.makeText(this,"You have to click an album",Toast.LENGTH_SHORT).show();
            return;
        }

        for (int i = 0; i < AlbumsList.user.getAlbums().get(itemIndex).getPhotos().size(); i++){
            if (AlbumsList.user.getAlbums().get(itemIndex).getPhotos() != null){
                if (photoUrl.compareTo(AlbumsList.user.getAlbums().get(itemIndex).getPhotos().get(i).getUriPath()) == 0){
                    String output = "Photo already exists in : " + AlbumsList.user.getAlbums().get(itemIndex).getName();
                    Toast.makeText(this,output,Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        }
        //Toast.makeText(this,"Reach the end",Toast.LENGTH_SHORT).show();
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            switch (which){
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
                case DialogInterface.BUTTON_POSITIVE:
                    move();
                    break;
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Move this photo?")
                .setNegativeButton("No", dialogClickListener)
                .setPositiveButton("Yes", dialogClickListener)
                .show();
    }

    private void move(){
        ModelPhoto newPhoto = new ModelPhoto(photoUrl);
        if (locationTag != null){
            for (int i = 0; i < locationTag.size(); i++){
                newPhoto.addTag("Location",locationTag.get(i));
            }
        }
        if (personTag != null){
            for (int i = 0; i < personTag.size(); i++){
                newPhoto.addTag("Person",personTag.get(i));
            }
        }
        AlbumsList.user.getAlbums().get(itemIndex).getPhotos().add(newPhoto);
        AlbumsList.user.getAlbums().get(albumIndex).getPhotos().remove(photoIndex);
        try {
            User.write(AlbumsList.user);
        } catch (IOException e) {
            Toast.makeText(this,"Going Wrong Here",Toast.LENGTH_SHORT).show();
        }
        setResult(RESULT_OK);
        finish();
    }
}
