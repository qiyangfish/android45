package com.example.photosapp;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.viewpager.widget.PagerAdapter;

import java.util.ArrayList;

public class PhotoAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<ModelPhoto> data;

    PhotoAdapter(Context context, ArrayList<ModelPhoto> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
        imageView.setAdjustViewBounds(true);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        //imageView.setElevation(4 * context.getResources().getDisplayMetrics().density);
        Uri imageUri = Uri.parse(data.get(position).getUriPath());
        imageView.setImageURI(imageUri);
        container.addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }
}
