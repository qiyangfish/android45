package com.example.photosapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;


public class PhotoSearch extends AppCompatActivity {
    public static final int OR_PER_MODE = 1;
    public static final int OR_LOC_MODE = 2;
    public static final int AND_MODE = 3;
    public static final String LOCATION = "Location";
    public static final String PERSON = "Person";
    private EditText search_name;
    private EditText search_location;
    private SearchAdapter adapter;
    private GridView gridView;
    private ArrayList<ModelPhoto> ans = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_search);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle("Search photos");

        search_name = findViewById(R.id.search_name);
        search_location = findViewById(R.id.search_location);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void save(View view){
        if (ans.size() == 0){
            Toast.makeText(this,"Nothing to save",Toast.LENGTH_SHORT).show();
            return;
        }
        albumAdding();
        //finish();
    }

    private boolean albumAdding() {
        //Toast.makeText(this,"In adding",Toast.LENGTH_SHORT).show();
        Bundle bundle = new Bundle();
        bundle.putString(AlbumsList.AlBUM_NAME, "");
        Intent intent = new Intent(this, AlbumAdd.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, AlbumsList.ADD_ALBUM_CODE);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //Toast.makeText(this,"In creating",Toast.LENGTH_SHORT).show();
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            return;
        }
        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }
        String name = bundle.getString(AlbumModify.AlBUM_NAME);
        if (requestCode == AlbumsList.ADD_ALBUM_CODE) {
            ModelAlbum album = new ModelAlbum(name);
            album.setPhotos(ans);
            AlbumsList.user.getAlbums().add(album);
            try {
                User.write(AlbumsList.user);
            } catch (IOException e) {
                Toast.makeText(this,"Going Wrong Here",Toast.LENGTH_SHORT).show();
            }
        }
        Bundle newBundle = new Bundle();
        Intent newIntent = new Intent();
        newIntent.putExtras(newBundle);
        setResult(RESULT_OK,newIntent);
        finish();
    }

    public void and_search(View view) {
        String perTag = search_name.getText().toString();
        String locTag = search_location.getText().toString();
        if (perTag.length() == 0 || locTag.length() == 0) {
            Toast.makeText(this,"You have to give a name",Toast.LENGTH_SHORT).show();
            return; // does not quit activity, just returns from method
        }
        ans = new ArrayList<>();
        gridView = findViewById(R.id.photo_search_grid);
        adapter = new SearchAdapter(this, getPhotos(AND_MODE,PERSON,LOCATION,perTag,locTag));
        gridView.setAdapter(adapter);
        search_name.setText("");
        search_location.setText("");
    }


    public void or_search(View view){
        String perTag = search_name.getText().toString();
        String locTag = search_location.getText().toString();
        if (perTag.length() == 0 && locTag.length() == 0) {
            Toast.makeText(this,"You have to give a name",Toast.LENGTH_SHORT).show();
            return;
        }
        ans = new ArrayList<>();
        if (perTag.length() != 0){
            gridView = findViewById(R.id.photo_search_grid);
            adapter = new SearchAdapter(this, getPhotos(OR_PER_MODE,PERSON,null,perTag,null));
            gridView.setAdapter(adapter);
        } else{
            gridView = findViewById(R.id.photo_search_grid);
            adapter = new SearchAdapter(this, getPhotos(OR_LOC_MODE,null,LOCATION,null,locTag));
            gridView.setAdapter(adapter);
        }
        search_name.setText("");
        search_location.setText("");
    }

    private ArrayList<ModelPhoto> getPhotos(int mode, String keyPer,String keyLoc, String valuePer,String valueLoc){
        User user = AlbumsList.user;

        for (int i = 0; i < user.getAlbums().size(); i++){
            for (int j = 0; j < user.getAlbums().get(i).getPhotos().size(); j++){
                if (mode == OR_PER_MODE){
                    if (user.getAlbums().get(i).getPhotos().get(j).getSearchResult(keyPer,valuePer) != null){
                        if (!ifExist(user.getAlbums().get(i).getPhotos().get(j).getUriPath(),ans)){
                            ans.add(user.getAlbums().get(i).getPhotos().get(j));
                        }
                    }
                } else if (mode == OR_LOC_MODE){
                    if (user.getAlbums().get(i).getPhotos().get(j).getSearchResult(keyLoc,valueLoc) != null){
                        if (!ifExist(user.getAlbums().get(i).getPhotos().get(j).getUriPath(),ans)){
                            ans.add(user.getAlbums().get(i).getPhotos().get(j));
                        }
                    }
                } else{
                    if (user.getAlbums().get(i).getPhotos().get(j).getSearchResult(keyPer,valuePer) != null
                    && user.getAlbums().get(i).getPhotos().get(j).getSearchResult(keyLoc,valueLoc) != null){
                        if (!ifExist(user.getAlbums().get(i).getPhotos().get(j).getUriPath(),ans)){
                            ans.add(user.getAlbums().get(i).getPhotos().get(j));
                        }
                    }
                }
            }
        }
        return ans;
    }

    private boolean ifExist(String Url, ArrayList<ModelPhoto> arrayList){
        for (int i = 0; i < arrayList.size(); i ++){
            if (Url.compareTo(arrayList.get(i).getUriPath()) == 0){
                return true;
            }
        }
        return false;
    }
}
