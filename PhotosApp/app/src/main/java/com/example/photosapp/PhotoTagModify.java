package com.example.photosapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;

public class PhotoTagModify extends AppCompatActivity {

    public static final String TAG_NAME = "tag_name";
    public static final String TAG_VALUE = "tag_value";
    public static final String PHOTO_INDEX = "photo_index";
    private int PhotoListIndex;
    private String newTagName;
    private EditText newTagValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // get the fields
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_tag_modification);
        newTagValue= findViewById(R.id.tag_new_value);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Create a tag");

        Spinner spinner = findViewById(R.id.tag_spinner);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.tag_name_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                newTagName=spinner.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                newTagName = "Person";
            }
        });

        // see if info was passed in to populate fields
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            PhotoListIndex = bundle.getInt(PHOTO_INDEX);
            //albumName.setText(bundle.getString(AlBUM_NAME));
        }
        Log.d("tag", String.valueOf(PhotoListIndex));
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void save(View view) {
        // gather all data from text fields
        //BUG: Duplicate tag name?
        String name = newTagName;
        Log.d("tag", name);
        String value = newTagValue.getText().toString();
        // pop up dialog if errors in input, and return
        // BUG:
        if (value == null || value.length() == 0 ) {
            DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        break;
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Please type something for your tag")
                    .setPositiveButton("Ok", dialogClickListener)
                    .show();
            return;
        }
        // make Bundle
        Bundle bundle = new Bundle();
        bundle.putString(TAG_VALUE, value);
        bundle.putString(TAG_NAME, name);
        //Log.d("tag", name);
        // send back to caller
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);

        finish(); // pops activity from the call stack, returns to parent
    }
}
