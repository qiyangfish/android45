package com.example.photosapp;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.viewpager.widget.ViewPager;

import java.io.IOException;
import java.util.ArrayList;

public class PhotoView extends AppCompatActivity {

    private ViewPager photoSlide;
    public static final String THUMBNAIL_INDEX = "thumbnail_index";
    public static final String PHOTO_INDEX = "photo_index";

    public static final String ALBUM_INDEX = "album_index";
    private int photoViewIndex;
    private ListView tagList;

    public static final String TAG_NAME = "tag_name";
    public static final String TAG_VALUE = "tag_value";
    public static final int ADD_TAG_CODE = 1;
    public static final int MOVE_PHOTO_CODE = 2;

    //MUST HAVE
    public static final String EXTRA_INITIAL_URI="android.provider.extra.INITIAL_URI";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Ringtone r= RingtoneManager.getRingtone(this, uri);
        getSupportActionBar().setTitle("Photo Display");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            photoViewIndex = bundle.getInt(THUMBNAIL_INDEX);
            //albumViewIndex = bundle.getInt(ALBUM_INDEX);
        }
        photoSlide = findViewById(R.id.photo_viewpager);
        PhotoAdapter adapter = new PhotoAdapter(this, getPhotos());
        photoSlide.setAdapter(adapter);
        photoSlide.setCurrentItem(photoViewIndex);
        photoSlide.setPageTransformer(true, new ZoomOutPageTransformer());
        photoSlide.addOnPageChangeListener(new ViewPager.OnPageChangeListener(){
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.d("tag1", String.valueOf(position));
            }

            @Override
            public void onPageSelected(int position) {
                Log.d("tag2", String.valueOf(position));
                refreshTag(position);
                photoViewIndex = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        tagList = findViewById(R.id.tag_list);

        String[] tagMap = getPhotos().get(photoViewIndex).getTag();
        ArrayAdapter<String> testadapter =
                new ArrayAdapter<>(this, R.layout.album_list_textfield, tagMap);
        tagList.setAdapter(testadapter);
        tagList.setOnItemLongClickListener(
                (p,v,pos,id) -> tagDeletion(pos));
    }

    public void refreshTag(int position){
        String[] tagMap = getPhotos().get(position).getTag();
        ArrayAdapter<String> testadapter =
                new ArrayAdapter<>(this, R.layout.album_list_textfield, tagMap);
        tagList.setAdapter(testadapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tag_modify, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tag_add:
                return tagAddition();
            //case R.id.photo_move:
            //    return movePhoto();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean tagAddition() {
        Bundle bundle = new Bundle();
        bundle.putInt(PHOTO_INDEX, photoViewIndex);
        Intent intent = new Intent(this, PhotoTagModify.class);
        intent.putExtras(bundle);
        //calling onActivityResult when child finish
        startActivityForResult(intent, ADD_TAG_CODE);
        return true;
    }




    private boolean tagDeletion(int pos) {
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    //Yes button clicked
                    String tempTag = getPhotos().get(photoViewIndex).getTag()[pos];
                    String name = "Location";
                    if(tempTag.charAt(0)=='P'){
                        name = "Person";
                    }
                    String value = "";
                    Log.d("tag1", String.valueOf(tempTag.indexOf(" ")));
                    value = tempTag.substring(tempTag.indexOf(" ")+1);
                    Log.d("tag", value);
                    //String value = bundle.getString(TAG_VALUE);
                    getPhotos().get(photoViewIndex).removeTag(name, value);
                    //}
                    // redo the adapter to reflect change
                    try {
                        User.write(AlbumsList.user);
                    } catch (IOException e) {
                        Toast.makeText(this,"Something Going Wrong",Toast.LENGTH_SHORT).show();
                    }
                    String[] tagMap = getPhotos().get(photoViewIndex).getTag();
                    //Log.d("tag", tagMap[0]);
                    ArrayAdapter<String> testadapter =
                            new ArrayAdapter<>(this, R.layout.album_list_textfield, tagMap);
                    tagList.setAdapter(testadapter);
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    break;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Delete this tag?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            return;
        }

        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }

        // gather all info passed back by launched activity
        if (requestCode == ADD_TAG_CODE){
            String name = bundle.getString(TAG_NAME);
            String value = bundle.getString(TAG_VALUE);
            getPhotos().get(photoViewIndex).addTag(name, value);

            Log.d("photoViewIndex:", String.valueOf(photoViewIndex));

            try {
                User.write(AlbumsList.user);
            } catch (IOException e) {
                Toast.makeText(this,"Something Going Wrong",Toast.LENGTH_SHORT).show();
            }

            String[] tagMap = getPhotos().get(photoViewIndex).getTag();
            //Log.d("tag", tagMap[0]);
            ArrayAdapter<String> testadapter =
                    new ArrayAdapter<>(this, R.layout.album_list_textfield, tagMap);
            tagList.setAdapter(testadapter);
        }

    }

    private ArrayList<ModelPhoto> getPhotos(){
        int index = getIntent().getIntExtra(AlbumsList.ALBUM_INDEX, 0);
        if (AlbumsList.user.getAlbums() == null){
            return null;
        }
        //Log.d("Index of the selected album", Integer.toString(index));
        return AlbumsList.user.getAlbums().get(index).getPhotos();
    }

    public static class ZoomOutPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.85f;
        private static final float MIN_ALPHA = 0.5f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();
            int pageHeight = view.getHeight();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0f);

            } else if (position <= 1) { // [-1,1]
                // Modify the default slide transition to shrink the page as well
                float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                if (position < 0) {
                    view.setTranslationX(horzMargin - vertMargin / 2);
                } else {
                    view.setTranslationX(-horzMargin + vertMargin / 2);
                }

                // Scale the page down (between MIN_SCALE and 1)
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

                // Fade the page relative to its size.
                view.setAlpha(MIN_ALPHA +
                        (scaleFactor - MIN_SCALE) /
                                (1 - MIN_SCALE) * (1 - MIN_ALPHA));

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0f);
            }
        }
    }
}