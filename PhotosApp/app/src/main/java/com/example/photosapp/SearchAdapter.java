package com.example.photosapp;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class SearchAdapter extends ArrayAdapter {

    private Context mContext;
    private ArrayList<ModelPhoto> data;

    public SearchAdapter(Context context, ArrayList<ModelPhoto> data){
        super(context, R.layout.photo_search, data);
        this.mContext = context;
        this.data = data;
    }

    public int getCount() {
        return data.size();
    }
    public Object getItem(int position) {
        return null;
    }
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View currentView, ViewGroup parent){
        ImageView imageView = new ImageView(mContext);
        imageView.setLayoutParams(new GridView.LayoutParams(250, 250));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setPadding(40, 8, 40, 8);
        Uri imageUri = Uri.parse(data.get(position).getUriPath());
        imageView.setImageURI(imageUri);
        return imageView;
    }
}