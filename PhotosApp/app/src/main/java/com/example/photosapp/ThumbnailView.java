package com.example.photosapp;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.util.ArrayList;

public class ThumbnailView extends AppCompatActivity {

    private static final int RESULT_LOAD_IMAGE = 5;
    private GridView gridView;
    public static final String THUMBNAIL_INDEX = "thumbnail_index";
    public static final String AlBUM_NAME = "album_name";
    public static final String ALBUM_INDEX = "album_index";
    public static final int PHOTO_VIEW = 2;

    private ThumbnailAdapter adapter;
    private static final int FILE_SELECT_CODE = 0;
    private ModelAlbum currentAlbum;

    private static final int SELECT_PHOTO = 1;
    //MUST HAVE
    public static final String EXTRA_INITIAL_URI="android.provider.extra.INITIAL_URI";

    public static final String PHOTO_URL = "photo_url";
    public static final String PHOTO_LOCATION_TAG = "photo_location_TAG";
    public static final String PHOTO_PERSON_TAG = "photo_person_tag";
    private int albumViewIndex;
    public static final String PHOTO_INDEX = "photo_index";
    public static final int MOVE_PHOTO_CODE = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thumbnail_view);
        // other auto-generated stuff
        // activate the “up”/”back” arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // get the name and detail from bundle
        Bundle bundle = getIntent().getExtras();
        // return from photoView will cause null pointer
        String albumName = bundle.getString(AlBUM_NAME);
        //BUG
        getSupportActionBar().setTitle(albumName);
        int index = getIntent().getIntExtra(AlbumsList.ALBUM_INDEX, 0);
        albumViewIndex = index;
        //currentAlbum = AlbumsList.user.getAlbums().get(index);

        gridView = findViewById(R.id.thumbnails_grid);
        //adapter sending a arraylist of photos
        adapter = new ThumbnailAdapter(this, getPhotos());

        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(
                (p,v,pos,id) -> photoView(pos));

        gridView.setOnItemLongClickListener(
                (p,v,pos,id) -> thumbnailDelete(pos));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Allow user to select photo

                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.setType("image/*");
                startActivityForResult(intent, SELECT_PHOTO);

                gridView.setAdapter(adapter);
                // Add to current album
            }
        });
    }

    private boolean thumbnailDelete(int pos) {
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    //Yes button clicked
                    getPhotos().remove(pos);
                    //}
                    // redo the adapter to reflect change
                    try {
                        User.write(AlbumsList.user);
                    } catch (IOException e) {
                        Toast.makeText(this,"Something Going Wrong",Toast.LENGTH_SHORT).show();
                    }
                    //global adapter
                    adapter = new ThumbnailAdapter(this, getPhotos());
                    gridView.setAdapter(adapter);
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    movePhoto(pos);
                    break;
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("What would you like to do with this photo?").setPositiveButton("Delete it", dialogClickListener)
                .setNegativeButton("Move it", dialogClickListener).setNeutralButton("None", dialogClickListener).show();
        return true;
    }

    private void photoView(int pos) {
        //Log.d("tag", String.valueOf(pos));
        Bundle bundle = new Bundle();
        bundle.putInt(ALBUM_INDEX,getIntent().getIntExtra(AlbumsList.ALBUM_INDEX, 0));
        bundle.putInt(THUMBNAIL_INDEX, pos);
        //bundle.putString(AlBUM_NAME, albumName);
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setClass(this, PhotoView.class);
        intent.putExtras(bundle);
        //intent.putExtra(ALBUM_INDEX,pos);
        startActivityForResult(intent, PHOTO_VIEW);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_delete_album, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.album_delete) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    */

    private boolean movePhoto(int pos) {
        Bundle bundle = new Bundle();
        bundle.putInt(PHOTO_INDEX, pos);
        bundle.putInt(ALBUM_INDEX, albumViewIndex);
        bundle.putString(PHOTO_URL, getPhotos().get(pos).getUriPath());
        bundle.putStringArrayList(PHOTO_LOCATION_TAG, getPhotos().get(pos).getMap().get("Location"));
        bundle.putStringArrayList(PHOTO_PERSON_TAG, getPhotos().get(pos).getMap().get("Person"));
        Intent intent = new Intent(this, MovePhoto.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, MOVE_PHOTO_CODE);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //BUG: DUPLICATE?
        super.onActivityResult(requestCode,resultCode,intent);
        if(resultCode == RESULT_OK){
            if (requestCode == SELECT_PHOTO){
                Uri uri = intent.getData();
                String photoUriPath = uri.toString();

                for(int duplicateCheck = 0; duplicateCheck < getPhotos().size(); duplicateCheck++){
                    if(getPhotos().get(duplicateCheck).getUriPath().equals(photoUriPath)){
                        Toast.makeText(this,"Duplicate Photo",Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                ModelPhoto photo = new ModelPhoto(photoUriPath);
                int index = getIntent().getIntExtra(AlbumsList.ALBUM_INDEX, 0);

                //String output = "Index is: " + index;
                //Toast.makeText(this,output,Toast.LENGTH_SHORT).show();

                if (photo.getMap() != null){
                    Log.d("Ans of Search", "There indeed something in the map");
                } else {
                    Log.d("Ans of Search", "There is nothing in the map");
                }
                this.getPhotos().add(photo);

                try {
                    User.write(AlbumsList.user);
                } catch (IOException e) {
                    Toast.makeText(this,"Something Going Wrong",Toast.LENGTH_SHORT).show();
                }
                adapter = new ThumbnailAdapter(this,  getPhotos());
                adapter.notifyDataSetChanged();
                gridView.setAdapter(adapter);
            }
            if (requestCode == PHOTO_VIEW){
                adapter = new ThumbnailAdapter(this,  getPhotos());
                adapter.notifyDataSetChanged();
                gridView.setAdapter(adapter);
            }
            if(requestCode == MOVE_PHOTO_CODE){
                adapter = new ThumbnailAdapter(this,  getPhotos());
                adapter.notifyDataSetChanged();
                gridView.setAdapter(adapter);
            }
        }

    }

    private ArrayList<ModelPhoto> getPhotos(){
        int index = getIntent().getIntExtra(AlbumsList.ALBUM_INDEX, 0);
        Log.d("Index of the selected ALBUM", Integer.toString(index));
        return AlbumsList.user.getAlbums().get(index).getPhotos();
    }
}
