package com.example.photosapp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {
    private ArrayList<ModelAlbum> albums;
    //public static final String storeDir = "file";
    public static final String storeFile = "/data/data/com.example.photosapp/file/photos.dat";

    public User() {this.albums = new ArrayList<>();}

    public User(ArrayList<ModelAlbum> albums){
        this.albums = albums;
    }

    public ArrayList<ModelAlbum> getAlbums(){return albums;}

    public void setAlbums(ArrayList<ModelAlbum> albums){this.albums = albums;}

    public static User read() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(storeFile));
        User user = (User) ois.readObject();
        ois.close();
        return user;
    }


    public static void write (User user) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(storeFile));
        oos.writeObject(user);
        oos.close();
    }
}
